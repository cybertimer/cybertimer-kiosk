/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains the initialization part
// Prefix: lod

/** Load configuration file.
 * @param success Function called in case of success (no arguments)
 * @param error Function called in case of error (args: code, message)
 */
function lod_loadConfig(success, error) {
	jQuery.getJSON("./config.json")
		.done(function (data) {
			if ("server" in data) {
				CFG_HOST = data['server'];
			} else {
				error(500, "No server defined");
				return;
			}
			if ("port" in data) {
				CFG_PORT = data['port'];
				// else use default
			}
			if ("session-list-width" in data) {
				CFG_SESSION_LIST_WIDTH = data['session-list-width'];
			}
			if ("credit-help" in data) {
				CFG_CREDIT_HELP = data['credit-help'];
			}
			if ("gamepad-coin" in data) {
				CFG_GAMEPAD_COIN = data['gamepad-coin'];
			}
			if ("gamepad-credit" in data) {
				CFG_GAMEPAD_CREDIT = data['gamepad-credit'];
			}
			// configuration ready, try to get station list
			lod_getStationList(success, error);
		})
		.fail(function (jqxhr, textStatus, err) {
			error(500, "Failed to load config file");
		});
}

function lod_getStationList(success, error) {
	srv_cmdList(function(stations) {
		// Success
		success(stations);
	},
	function(code, err) {
		// Error
		error(code, err);
	});
}