/* Copyright (C) 2016, see AUTHOR for contributors
 *
 * This file is part of CyberTimer.
 *
 * CyberTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CyberTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.
 */

// Widget for a session.
// prefix wssn

/** Create a new Session Widget. */
function wssn_new(session) {
	var html = "<li class=\"session_widget\" onclick=\"javacript:wssn_click('" + session.address + "');\">"
		+ "<p class=\"session_widget_name\">" + escHtml(session.name) + "</p>"
		+ "<p class=\"session_widget_time\">00:00</p>"
		+ "</li>";
	return {
		"session": session,
		"elem": jQuery(html)
	};
}

function wssn_append(widget, elem) {
	elem.append(widget.elem);
	wssn_update(widget);
}

function wssn_update(widget) {
	var session = widget.session;
	// Update state icon
	var wclass = "";
	if (session.unavailable) {
		wclass = "session_widget_unavailable";
	} else {
		switch (session.state) {
		case SSN_STATE_RUNNING:
		case SSN_STATE_STARTING:
			wclass = "session_widget_running";
			break;
		case SSN_STATE_IDLE:
		case SSN_STATE_STOPPING:
			wclass = "session_widget_available";
			break;
		case SSN_STATE_PAUSED:
		case SSN_STATE_PAUSING:
			wclass = "session_widget_paused";
			break;
		}
	}
	widget.elem.removeClass("session_widget_unavailable session_widget_running session_widget_available session_widget_paused");
	widget.elem.addClass(wclass);
	// Update time
	var time = "";
	if (session.ascending) {
		widget.elem.find("p.session_widget_time").addClass("ascending");
		time = "illimité";
	} else {
		widget.elem.find("p.session_widget_time").removeClass("ascending");
		var hours = Math.floor(session.remainingTime / 3600);
		var minutes = Math.floor(session.remainingTime / 60) % 60;
		minutes = Math.round(minutes / 5) * 5; // Step of 5 minutes
		if (minutes < 10) {
			minutes = "0" + minutes;
		} else if (minutes == 60) {
			hours++;
			minutes = "00";
		}
		time = hours + "h" + minutes;
		widget.elem.find("p.session_widget_time").html(time);
	}
}

function wssn_click(address) {
	dat_creditSession(address);
}