/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains the configuration mechanics
// Prefix: cfg

var CFG_HOST = null;
var CFG_PORT = 9234; // Default port
var CFG_SESSION_LIST_WIDTH = 5;
var CFG_CREDIT_HELP = null;

var CFG_GAMEPAD_COIN = null;
var CFG_GAMEPAD_CREDIT = null;