
 /* Copyright (C) 2016, see AUTHOR for contributors
 *
 * This file is part of CyberTimer.
 *
 * CyberTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CyberTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.
 */

// This file contains generic tools not directly bound to Cybertimer

String.prototype.format = function (args) {
	var str = this;
	return str.replace(String.prototype.format.regex, function(item) {
		var intVal = parseInt(item.substring(1, item.length - 1));
		var replace;
		if (intVal >= 0) {
			replace = args[intVal];
		} else if (intVal === -1) {
			replace = "{";
		} else if (intVal === -2) {
			replace = "}";
		} else {
			replace = "";
		}
		return replace;
	});
};
String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

String.isString = function (variable) {
	return (typeof(variable) == "string");
}

function isDefined(varName) {
	return typeof(window[varName]) != "undefined";
}

function escHtml(string) {
	return string;
}
function escAttr(string) {
	return string;
}
function escUrl(string) {
	return string;
}
/** Escape string to include it in javascript code, surrounded by ' '
 * and by " " for html element. */
function escJs(string) {
	var esc = string.replace(new RegExp("'", 'g'), "\\'");
	esc = esc.replace(new RegExp("\"", 'g'), "&quot;");
	return esc;
}