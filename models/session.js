/* Copyright (C) 2016, see AUTHOR for contributors
 *
 * This file is part of CyberTimer.
 *
 * CyberTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CyberTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.
 */

var SSN_STATE_RUNNING = "running";
var SSN_STATE_STARTING = "starting";
var SSN_STATE_PAUSED = "paused";
var SSN_STATE_PAUSING = "pausing"
var SSN_STATE_IDLE = "idle";
var SSN_STATE_STOPPING = "stopping"

/** Create a new session */
function ssn_new(address, name) {
	return {"name": name,
		"remainingTime": 0,
		"state": SSN_STATE_IDLE,
		"address": address,
		"ascending": false,
		"unavailable": true
	};
}

/** Update a session with incoming values. */
function ssn_update(session, newValues) {
	if (session.address != newValues.address) {
		return false;
	}
	session.state = newValues.state;
	session.remainingTime = newValues.remainingTime;
	session.ascending = newValues.ascending;
	session.unavailable = newValues.unavailable;
	return true;
}