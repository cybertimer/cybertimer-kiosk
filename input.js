/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains keyboard and gamepad management

// prefix: inp

var INP_ENTER = "Enter";
var INP_SPACE = " ";
var INP_ESC = "Escape";
var INP_ARROWUP = "ArrowUp";
var INP_ARROWDOWN = "ArrowDown";
var INP_ARROWLEFT = "ArrowLeft";
var INP_ARROWRIGHT = "ArrowRight";

var _inp_kbdbindings = {};

/** Initialize the keyboard handler. */
function inp_setup() {
	jQuery("html").keydown(inp_handleKey);
}

function inp_registerKey(key, callback) {
	_inp_kbdbindings[key] = callback;
}

function inp_dropKey(key) {
	delete _inp_kbdbindings[key];
}

function inp_handleKey(event) {
	console.info(event);
	var char = event["key"];
	if (char in _inp_kbdbindings) {
		_inp_kbdbindings[char]();
	}
}

/** Button trigger state: 0 unpressed, 1 pressed, 2 pressed and proceeded */
var gmp_inputTrigger = {"up": 0, "down": 0, "left": 0, "right": 0, "coin": 0, "credit": 0};
var gmp_gamepads = [];
var gmp_gamepad = null;

function gmp_buttonLoop() {
	if (gmp_gamepad != null) {
		if (gmp_gamepad.axes[0] < -0.5 && gmp_inputTrigger['left'] == 0) {
			gmp_inputTrigger["left"] = 1;
		} else if (gmp_gamepad.axes[0] > 0.5  && gmp_inputTrigger['right'] == 0) {
			gmp_inputTrigger["right"] = 1;
		} else if (gmp_gamepad.axes[0] > -0.5 && gmp_gamepad.axes[0] < 0.5) {
			if (gmp_inputTrigger["left"] == 2) {
				gmp_inputTrigger["left"] = 0;
			}
			if (gmp_inputTrigger["right"] == 2) {
				gmp_inputTrigger["right"] = 0;
			}
		}
		if (gmp_gamepad.axes[1] < - 0.5 && gmp_inputTrigger['up'] == 0) {
			gmp_inputTrigger["up"] = 1;
		} else if (gmp_gamepad.axes[1] > 0.5 && gmp_inputTrigger['down'] == 0) {
			gmp_inputTrigger["down"] = 1;
		} else if (gmp_gamepad.axes[1] > -0.5 && gmp_gamepad.axes[1] < 0.5) {
			if (gmp_inputTrigger["up"] == 2) {
				gmp_inputTrigger["up"] = 0;
			}
			if (gmp_inputTrigger["down"] == 2) {
				gmp_inputTrigger["down"] = 0;
			}
			
		}
		if (gmp_gamepad.buttons[CFG_GAMEPAD_COIN].pressed  && gmp_inputTrigger['coin'] == 0) {
			gmp_inputTrigger['coin'] = 1;
		} else if (!gmp_gamepad.buttons[CFG_GAMEPAD_COIN].pressed && gmp_inputTrigger['coin'] == 2) {
			gmp_inputTrigger['coin'] = 0;
		}
		if (gmp_gamepad.buttons[CFG_GAMEPAD_CREDIT].pressed  && gmp_inputTrigger['credit'] == 0) {
			gmp_inputTrigger['credit'] = 1;
		} else if (!gmp_gamepad.buttons[CFG_GAMEPAD_CREDIT].pressed && gmp_inputTrigger['credit'] == 2) {
			gmp_inputTrigger['credit'] = 0;
		}
	}
}

function gmp_gamepadLoop() {
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
	if (gamepads.length > 0) {
		gmp_gamepad = gamepads[0];
	} else {
		gmp_gamepad = null;
	}
	if (gmp_inputTrigger['coin'] == 1) {
		dat_insertCoin();
		gmp_inputTrigger['coin'] = 2;
	}
	if (gmp_inputTrigger['credit'] == 1) {
		dat_creditSelection();
		gmp_inputTrigger['credit'] = 2;
	}
	if (gmp_inputTrigger['up'] == 1) {
		if (dat_selectUp()) { dat_updateSelection(); }
		gmp_inputTrigger['up'] = 2;
	}
	if (gmp_inputTrigger['down'] == 1) {
		if (dat_selectDown()) { dat_updateSelection(); }
		gmp_inputTrigger['down'] = 2;
	}
	if (gmp_inputTrigger['left'] == 1) {
		if (dat_selectLeft()) { dat_updateSelection(); }
		gmp_inputTrigger['left'] = 2;
	}
	if (gmp_inputTrigger['right'] == 1) {
		if (dat_selectRight()) { dat_updateSelection(); }
		gmp_inputTrigger['right'] = 2;
	}
}

function gmp_setup() {
	setInterval(gmp_gamepadLoop, 250);
	setInterval(gmp_buttonLoop, 50);
}