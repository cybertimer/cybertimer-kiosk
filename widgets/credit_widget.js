/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains the credit state
// prefix: wcrd

/** Create a new Session Widget. */
function wcrd_new() {
	var html = "<p class=\"credit_widget\">INSERT COIN</p>";
	return {
		"elem": jQuery(html),
		"blinker": null,
	};
}

function wcrd_append(widget, elem) {
	elem.append(widget.elem);
}

function crd_updateCredits(widget, credits) {
	if (credits > 0) {
		var text = "CREDIT(S) " + credits;
		if (CFG_CREDIT_HELP != null) {
			text += "<br /><span class=\"credit_help\">" + escHtml(CFG_CREDIT_HELP) + "</span>";
		}
		widget.elem.html(text);
		widget.elem.removeClass("credit_blink_on credit_blink_off");
		if (widget.blinker != null) {
			clearTimeout(widget.blinker);
			widget.blinker = null;
		}
	} else {
		widget.elem.html("INSERT COIN");
		if (widget.blinker != null) {
			clearTimeout(widget.blinker);
		}
		_crd_blink_on(widget);
	}
}

function _crd_blink_on(widget) {
	widget.elem.removeClass("credit_blink_off");
	widget.elem.addClass("credit_blink_on");
	widget.blinker = setTimeout(function() { _crd_blink_off(widget); }, 1000);
}
function _crd_blink_off(widget) {
	widget.elem.removeClass("credit_blink_on");
	widget.elem.addClass("credit_blink_off");
	widget.blinker = setTimeout(function() { _crd_blink_on(widget); }, 300);
}