/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains the server communication part
// Prefix: srv

/** Send list command to the server */
function srv_cmdList(success, error) {
	jQuery.getJSON("http://" + CFG_HOST + ":" + CFG_PORT + "?cmd=list")
		.done(function(data) {
			if (!Array.isArray(data)) {
				error(503, "Unexpected server answer");
				return
			}
			for (var i = 0; i < data.length; i++) {
				
			}
			success(data);
		})
		.fail(function (jqxhr, textStatus, err) {
			error(503, "Unable to contact server");
		});

}

function srv_cmdCredit(address, pulse, success, error) {
	jQuery.getJSON("http://" + CFG_HOST + ":" + CFG_PORT + "?cmd=credit&address=" + address + "&pulse=" + pulse)
		.done(function(data) {
			success(data);
		})
		.fail(function(jqxhr, textStatus, err) {
			error(503, "Unable to contact server");
		});
}