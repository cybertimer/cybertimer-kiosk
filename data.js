/* Cybertimer Kiosk
 * 
 * This file is part of Cybertimer
 * 
 * Cybertimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Cybertimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Cybertimer uses jQuery, licenced by the jQuery Fundation
 * under the MIT Licence (see https://jquery.org/license/)
 */
 
// This file contains the instance state
// prefix: dat

var dat_sessions = [];
var dat_sessionWidgets = [];
var dat_credits = 0;
var dat_creditWidget = null;
var dat_cursor = [-1, -1];

/** Init session widgets with dat_sessions data. */
function dat_initSessionWidgets() {
	for (var i = 0; i < dat_sessions.length; i++) {
		dat_sessionWidgets.push(wssn_new(dat_sessions[i]));
	}
}

function dat_updateSession(session) {
	for (var i = 0; i < dat_sessions.length; i++) {
		if (ssn_update(dat_sessions[i], session)) {
			wssn_update(dat_sessionWidgets[i]);
			break;
		}
	}	
}

function dat_getSelectedSession() {
	return dat_sessions[dat_cursor[0] + dat_cursor[1] * CFG_SESSION_LIST_WIDTH]
}

function dat_lockSelection() {
	jQuery(".cursor_on").removeClass("cursor_on");
	dat_cursor = [-1, -1];
	inp_dropKey(INP_ARROWUP);
	inp_dropKey(INP_ARROWDOWN);
	inp_dropKey(INP_ARROWLEFT);
	inp_dropKey(INP_ARROWRIGHT);
	inp_dropKey(INP_SPACE);
}

function dat_unlockSelection() {
	dat_cursor = [0, 0];
	inp_registerKey(INP_ARROWUP, function() { if (dat_selectUp()) dat_updateSelection(); } );
	inp_registerKey(INP_ARROWDOWN, function() { if (dat_selectDown()) dat_updateSelection(); } );
	inp_registerKey(INP_ARROWLEFT, function() { if (dat_selectLeft()) dat_updateSelection(); } );
	inp_registerKey(INP_ARROWRIGHT, function() { if (dat_selectRight()) dat_updateSelection(); } );
	inp_registerKey(INP_SPACE, dat_creditSelection);
	dat_updateSelection();
}

function _dat_getSelectedIndex() {
	return dat_cursor[0] + dat_cursor[1] * CFG_SESSION_LIST_WIDTH;
}

function dat_selectLeft() {
	if (dat_cursor[0] > 0) {
		dat_cursor[0] = dat_cursor[0] - 1;
		return true;
	} else if (dat_cursor[1] > 0) {
		dat_cursor[1] = dat_cursor[1] - 1;
		dat_cursor[0] = CFG_SESSION_LIST_WIDTH - 1;
		return true;
	}
	return false;
}
function dat_selectRight() {
	if (dat_cursor[0] < CFG_SESSION_LIST_WIDTH - 1
			&& _dat_getSelectedIndex() < dat_sessions.length - 1) {
		dat_cursor[0] = dat_cursor[0] + 1;
		return true;
	} else if (_dat_getSelectedIndex() < dat_sessions.length - 1) {
		dat_cursor[1] = dat_cursor[1] + 1;
		dat_cursor[0] = 0;
		return true;
	}
	return false;
}
function dat_selectDown() {
	if (dat_cursor[1] < Math.floor(dat_sessions.length / CFG_SESSION_LIST_WIDTH)) {
		dat_cursor[1] = dat_cursor[1] + 1;
		if (_dat_getSelectedIndex() >= dat_sessions.length - 1) {
			dat_cursor[0] = (dat_sessions.length - 1) % CFG_SESSION_LIST_WIDTH;
		}
		return true;
	}
	return false;
}
function dat_selectUp() {
	if (dat_cursor[1] > 0) {
		dat_cursor[1] = dat_cursor[1] - 1;
		return true;
	}
	return false;
}
function dat_updateSelection() {
	jQuery(".session_widget").removeClass("cursor_on");
	jQuery(".session_widget").eq(_dat_getSelectedIndex()).addClass("cursor_on");
}

function dat_insertCoin() {
	if (dat_credits == 0) {
		dat_unlockSelection();
	}
	dat_credits++;
	crd_updateCredits(dat_creditWidget, dat_credits);
	document.getElementById("audio_coin").play();
}

function dat_creditSelection() {
	if (dat_cursor[0] != -1) {
		dat_creditSession(dat_sessions[_dat_getSelectedIndex()].address);
	}
}

function dat_creditSession(address) {
	if (dat_credits > 0) {
		dat_credits--;
		srv_cmdCredit(address, 1,
		function(data) {
			document.getElementById("audio_credit").play();
			for (var i = 0; i < dat_sessions.length; i++) {
				if (ssn_update(dat_sessions[i], data)) {
					wssn_update(dat_sessionWidgets[i]);
					break;
				}
			}
			if (dat_credits == 0) {
				dat_lockSelection();
			}
		},
		function(code, err) {
			dat_credits++;
			crd_updateCredits(dat_creditWidget, dat_credits);
		});
		crd_updateCredits(dat_creditWidget, dat_credits);
	}
}

function dat_bindSessionWidgets(elem) {
	for (var i = 0; i < dat_sessionWidgets.length; i++) {
		var line;
		if (i % CFG_SESSION_LIST_WIDTH == 0) {
			line = jQuery("<ul class=\"session_list_line\"></ul>");
			elem.append(line);
		}
		wssn_append(dat_sessionWidgets[i], line);
	}
}